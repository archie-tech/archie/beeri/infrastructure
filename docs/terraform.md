# Archie Beeri Infrastructure

This GitLab project is used to store Terraform IaC configuration files for Archie Beeri application.

## Terminology and Taxonomy

*   Workload
    *   Definition
        *   A collection of `services` that delivers business value, such as a customer-facing application or a backend process. 
    *   Examples
        *   Document Ingestion
        *   User Authentication
*   Service
    *   Definition
        *   A collection of `resources` that implement a specific business logic in the `workload`. 
    *   Examples
        *   Document Ingestion - Incomming Queue
        *   Document Ingestion - Preprocessing Lambda
*   Resource
    *   Definition
        *   The basic infrastructure object that is used by `services`.
    *   Examples
        *   Document Ingestion - Preprocessing Lambda - Function
        *   Document Ingestion - Preprocessing Lambda - IAM Role
        *   Document Ingestion - Preprocessing Lambda - IAM Policy

## Terraform implementation

*   `Resources` maps directly to Terraform. 
*   When using Terraform with folder (child) modules, `services` are maped to files and `workloads` are maped to folders. Example:
    *   doc-ingest
        *   api-gw
        *   ecs
        *   lambda
        *   sqs
*   When using Terraform only with file-modules, there are 2 options to map the high level `service` and `workload` entities:
    *   Each `service` is maped to a single file. Grouping `services` into `workloads` is accomplished by file name conventions. Example:
        *   doc-ingest.api-gw
        *   doc-ingest.ecs
        *   doc-ingest.lambda
        *   doc-ingest.sqs
    *   Each `workload` is maped to a single file. Seperating `services` inside a `workload` is accomplished by the arrangement of resources in the file and using remarks.

## Terraform Modules

*   core - Terraform and AWS provider setup.
*   vpc - Public and private subnets and related resources.
*   frontend - Frontend web application, based on React JS library.
*   doc-ingest - Documents Ingestion workload.
*   doc-update - Documents update and deletion workload.
*   reports viewer
*   user management (create, update, delete)
*   user authenticaton and authorisation

## Usefull links

### Best Practices

* https://docs.aws.amazon.com/wellarchitected/latest/userguide/workloads.html
* https://martinfowler.com/articles/microservices.html
* https://smartlogic.io/blog/how-i-organize-terraform-modules-off-the-beaten-path/

### API GW Integration

* https://docs.aws.amazon.com/apigateway/latest/developerguide/apigateway-integrate-with-cognito.html
* https://docs.amplify.aws/lib/auth/getting-started/q/platform/js/#option-1-use-pre-built-ui-components
* https://docs.amplify.aws/lib/client-configuration/configuring-amplify-categories/q/platform/js/
* https://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-mapping-template-reference.html

### CORS

* https://docs.aws.amazon.com/apigateway/latest/developerguide/how-to-cors.html
