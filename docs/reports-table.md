# Reports Table Design Notes

Configuration options for DynamoDB table archie_beeri_reports

## Simple configuration

* hash key: report-month (example: 2022-09)
* range key: creation-time (example: 2022-09-17T08:43:16.826202)
* write patterns: creation-time makes the composite primary key unique. optionally use folder-id as filter when updating an item
* access patterns: select records based on month, sorted or filterred by creation-time

## Advanced configuration

* hash key: folder-id
* global secondary index
    * hash key: report-month
    * range key: creation-time
* write patterns: folder-id is unique by defenition (uuid)
* access patterns: use index to select records based on month, sorted or filterred by creation time