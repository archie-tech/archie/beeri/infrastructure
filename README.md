# Archie Beeri Infrastructure

## Usec Cases

![use cases](docs/diagrams/case.svg)

* Un-authenticated user (guest) can only search and view documents and files.
* User with an `editor` role can also update specific parts of a document (title, description).
* User with a `manager` role can update all parts of a document, and also ingest documents and files, felete documents and files and generate reports.
* Some actions are not yet implemented in Archie UI and can only be perform by an `adminsitrator` with direct access to AWS console or CLI.

## UI Overview

![ui components](docs/diagrams/ui_components.svg)

* Most users interact with the main WebApp, using their browser.
* Managers also have access to the Admin Webapp.
* Assets files (pdf, images, audio and video files) are stored in s3 buckets behind a cloud-front distribution and can be accessed directly from the user web browser.

## API Overview

![api components](docs/diagrams/api_components.svg)

* Most of the application logic is implemented by using Lambda non-proxy integration.
* RBAC is implemented by api gateway authorizer.
* Apache Solr search operations are exposed to users through http integration with Solr EC2 instance.

## Ingest Workload

![ingest workload](docs/diagrams/ingest_workload.svg)

## Update Workload

![update workload](docs/diagrams/update_workload.svg)