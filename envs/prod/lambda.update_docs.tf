resource "aws_lambda_function" "update_docs" {
  function_name = lower("${var.application}_${var.customer}_update_docs")
  role          = aws_iam_role.update_docs.arn
  runtime       = "python3.12"
  timeout       = 30
  handler       = "update.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      logging_level             = "INFO"
      solr_url                  = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri"
      archie_base64_credentials = var.archie_base64_credentials
      queue_url                 = aws_sqs_queue.update_docs.url
      public_assets_bucket      = var.public_assets_bucket
      private_assets_bucket     = var.private_assets_bucket
      secret_assets_bucket      = var.secret_assets_bucket
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_iam_role" "update_docs" {
  name = lower("${var.application}_${var.customer}_update_docs")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "update_docs_eni_management" {
  role       = aws_iam_role.update_docs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_lambda_permission" "update_docs" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.update_docs.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_region}:${var.aws_account}:${aws_api_gateway_rest_api.public.id}/*/${aws_api_gateway_method.patch_docs.http_method}${aws_api_gateway_resource.docs.path}"
}

resource "aws_iam_policy" "update_docs" {
  name = lower("${var.application}_${var.customer}_update_docs")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogGroup"
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : [
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_update_docs",
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_update_docs:log-stream:*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:GetQueueAttributes",
            "sqs:GetQueueUrl",
            "sqs:SendMessage",
            "sqs:SendMessageBatch"
          ],
          "Resource" : aws_sqs_queue.update_docs.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:ListBucket",
          ],
          "Resource" : [
            "arn:aws:s3:::${var.public_assets_bucket}",
            "arn:aws:s3:::${var.private_assets_bucket}",
            "arn:aws:s3:::${var.secret_assets_bucket}"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject",
            "s3:DeleteObject"
          ],
          "Resource" : [
            "arn:aws:s3:::${var.public_assets_bucket}/*",
            "arn:aws:s3:::${var.private_assets_bucket}/*",
            "arn:aws:s3:::${var.secret_assets_bucket}/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "update_docs" {
  role       = aws_iam_role.update_docs.name
  policy_arn = aws_iam_policy.update_docs.arn
}

resource "aws_sqs_queue" "update_docs" {
  name = lower("${var.application}_${var.customer}_update_docs")
  redrive_policy = jsonencode({
    "deadLetterTargetArn" = aws_sqs_queue.update_docs_dlq.arn,
    "maxReceiveCount"     = 1
  })
  tags = {
    Name = lower("${var.application}_${var.customer}_update_docs")
  }
}

resource "aws_sqs_queue" "update_docs_dlq" {
  name = lower("${var.application}_${var.customer}_update_docs_dlq")
  tags = {
    Name        = lower("${var.application}_${var.customer}_update_docs_dlq")
    Description = "Dead Letter Queue for update_docs queue"
  }
}
