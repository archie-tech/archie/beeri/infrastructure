resource "aws_api_gateway_rest_api" "public" {
  name                         = lower("${var.application}_${var.customer}_public")
  disable_execute_api_endpoint = true
}

resource "aws_api_gateway_resource" "docs" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  parent_id   = aws_api_gateway_rest_api.public.root_resource_id
  path_part   = "docs"
}

resource "aws_api_gateway_resource" "reports" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  parent_id   = aws_api_gateway_rest_api.public.root_resource_id
  path_part   = "reports"
}

resource "aws_api_gateway_resource" "folders" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  parent_id   = aws_api_gateway_rest_api.public.root_resource_id
  path_part   = "folders"
}

resource "aws_api_gateway_resource" "dropbox" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  parent_id   = aws_api_gateway_rest_api.public.root_resource_id
  path_part   = "dropbox"
}

resource "aws_api_gateway_deployment" "public" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  triggers = {
    redeployment = sha1(jsonencode([
      file("${path.module}/api_gateway_authorizer.tf"),
      file("${path.module}/api_gateway_method.docs.options.tf"),
      file("${path.module}/api_gateway_method.docs.get.tf"),
      file("${path.module}/api_gateway_method.docs.put.tf"),
      file("${path.module}/api_gateway_method.docs.patch.tf"),
      file("${path.module}/api_gateway_method.docs.delete.tf"),
      file("${path.module}/api_gateway_method.folders.get.tf"),
      file("${path.module}/api_gateway_method.folders.options.tf"),
      file("${path.module}/api_gateway_method.reports.get.tf"),
      file("${path.module}/api_gateway_method.reports.options.tf"),
      file("${path.module}/api_gateway_method.dropbox.options.tf"),
      file("${path.module}/api_gateway_method.dropbox.get.tf"),
      file("${path.module}/api_gateway_method.dropbox.delete.tf"),
      file("${path.module}/api_gateway_method.dropbox.post.tf"),
    ]))
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_api_gateway_stage" "public" {
  deployment_id      = aws_api_gateway_deployment.public.id
  rest_api_id        = aws_api_gateway_rest_api.public.id
  stage_name         = lower(var.environment)
  cache_cluster_size = 0.5
}

resource "aws_api_gateway_domain_name" "api" {
  domain_name              = var.api_domain_name
  regional_certificate_arn = aws_acm_certificate_validation.api.certificate_arn
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

resource "aws_api_gateway_base_path_mapping" "api" {
  api_id      = aws_api_gateway_rest_api.public.id
  stage_name  = aws_api_gateway_stage.public.stage_name
  domain_name = aws_api_gateway_domain_name.api.domain_name
}

resource "aws_route53_record" "api" {
  name    = aws_api_gateway_domain_name.api.domain_name
  zone_id = var.hosted_zone_id
  type    = "A"
  alias {
    name                   = aws_api_gateway_domain_name.api.regional_domain_name
    zone_id                = aws_api_gateway_domain_name.api.regional_zone_id
    evaluate_target_health = true
  }
}

resource "aws_acm_certificate" "api" {
  domain_name               = var.api_domain_name
  subject_alternative_names = ["*.${var.api_domain_name}"]
  validation_method         = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "api" {
  certificate_arn         = aws_acm_certificate.api.arn
  validation_record_fqdns = [for record in aws_route53_record.api_validation : record.fqdn]
}

resource "aws_route53_record" "api_validation" {
  for_each = {
    for dvo in aws_acm_certificate.api.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.hosted_zone_id
}
