# Object Ownership: ACLs disabled (Bucket owner enforced)
data "aws_s3_bucket" "private_assets" {
  bucket = var.private_assets_bucket
}

resource "aws_s3_bucket_versioning" "private_assets" {
  bucket = data.aws_s3_bucket.private_assets.id
  versioning_configuration {
    status = "Enabled"
  }
}

data "aws_s3_bucket" "private_assets_backup" {
  provider = aws.acm
  bucket   = "${var.private_assets_bucket}.backup"
}

resource "aws_s3_bucket_versioning" "private_assets_backup" {
  provider = aws.acm
  bucket   = data.aws_s3_bucket.private_assets_backup.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_cloudfront_distribution" "private_assets_bucket" {
  enabled     = true
  price_class = "PriceClass_200"
  #web_acl_id  = "arn:aws:wafv2:us-east-1:572361917465:global/webacl/beerionly/f9a3a634-d768-4d19-b4a0-d8e5b88408e0"
  web_acl_id = aws_wafv2_web_acl.archie_beeri.arn
  origin {
    domain_name = data.aws_s3_bucket.private_assets.bucket_regional_domain_name
    origin_id   = data.aws_s3_bucket.private_assets.bucket_regional_domain_name
    s3_origin_config {
      #origin_access_identity = "origin-access-identity/cloudfront/EKP61VV67EFBP"
      origin_access_identity = aws_cloudfront_origin_access_identity.archie_beeri_private_assets.cloudfront_access_identity_path
    }
  }
  default_cache_behavior {
    compress               = true
    target_origin_id       = data.aws_s3_bucket.private_assets.bucket_regional_domain_name
    viewer_protocol_policy = "https-only"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    allowed_methods        = ["GET", "HEAD"] # ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods         = ["GET", "HEAD"]
    cache_policy_id        = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    #origin_request_policy_id = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

resource "aws_cloudfront_origin_access_identity" "archie_beeri_private_assets" {
  comment = "access-identity-archie-beeri-private.s3.eu-west-1.amazonaws.com"
}

resource "aws_wafv2_web_acl" "archie_beeri" {
  provider = aws.acm
  #name     = lower("${var.application}_${var.customer}")
  name  = "beerionly"
  scope = "CLOUDFRONT"
  default_action {
    block {}
  }
  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "beerionly"
    sampled_requests_enabled   = true
  }
  rule {
    name     = "beerionly"
    priority = 0
    action {
      allow {
      }
    }
    statement {
      ip_set_reference_statement {
        #arn = "arn:aws:wafv2:us-east-1:572361917465:global/ipset/beerionly/eca0f17a-a394-4472-b85d-c2f4c6b0cb66"
        arn = aws_wafv2_ip_set.beeri.arn
      }
    }
    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "beerionly"
      sampled_requests_enabled   = true
    }
  }
}

resource "aws_wafv2_ip_set" "beeri" {
  provider           = aws.acm
  name               = "beerionly"
  scope              = "CLOUDFRONT"
  ip_address_version = "IPV4"
  addresses          = var.beeri_ip_cidr
}

resource "aws_s3_bucket_policy" "archie_beeri_private_assets" {
  bucket = data.aws_s3_bucket.private_assets.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Id" : "Beeri only access",
      "Statement" : [
        {
          "Sid" : "Beeri only access",
          "Effect" : "Allow",
          "Principal" : "*",
          "Action" : "s3:*",
          "Resource" : [
            data.aws_s3_bucket.private_assets.arn,
            "${data.aws_s3_bucket.private_assets.arn}/*"
          ],
          "Condition" : {
            "IpAddress" : {
              "aws:SourceIp" : var.beeri_ip_cidr
            }
          }
        },
        {
          "Sid" : "CloudFront access",
          "Effect" : "Allow",
          "Principal" : {
            #"AWS" : "arn:aws:iam::cloudfront:user/CloudFront Origin Access Identity EKP61VV67EFBP"
            "AWS" : aws_cloudfront_origin_access_identity.archie_beeri_private_assets.iam_arn
          },
          "Action" : "s3:GetObject",
          "Resource" : "${data.aws_s3_bucket.private_assets.arn}/*"
        }
      ]
    }
  )
}

resource "aws_s3_bucket_cors_configuration" "archie_beeri_private_assets" {
  bucket = data.aws_s3_bucket.private_assets.id
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = [
      "GET",
      "PUT",
      "POST",
      "DELETE",
      "HEAD"
    ]
    allowed_origins = ["https://${var.webapp_domain_name}"]
    expose_headers  = []
  }
}

resource "aws_iam_role" "private_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_private_assets_bucket_replication")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "s3.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "private_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_private_assets_bucket_replication")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetReplicationConfiguration",
            "s3:ListBucket",
          ]
          "Resource" : [data.aws_s3_bucket.private_assets.arn]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetObjectVersionForReplication",
            "s3:GetObjectVersionAcl",
            "s3:GetObjectVersionTagging",
          ]
          "Resource" : ["${data.aws_s3_bucket.private_assets.arn}/*"]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:ReplicateObject",
            #"s3:ReplicateDelete",
            "s3:ReplicateTags"
          ]
          "Resource" : ["${data.aws_s3_bucket.private_assets_backup.arn}/*"]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "private_assets_bucket_replication" {
  role       = aws_iam_role.private_assets_bucket_replication.name
  policy_arn = aws_iam_policy.private_assets_bucket_replication.arn
}

resource "aws_s3_bucket_replication_configuration" "private_assets" {
  depends_on = [
    aws_s3_bucket_versioning.private_assets,
    aws_s3_bucket_versioning.private_assets_backup
  ]
  role   = aws_iam_role.private_assets_bucket_replication.arn
  bucket = data.aws_s3_bucket.private_assets.id
  rule {
    id     = "private-1"
    status = var.bucket_replication_status
    destination {
      bucket        = data.aws_s3_bucket.private_assets_backup.arn
      storage_class = "DEEP_ARCHIVE"
    }
  }
}

# replicate existing objects
# aws s3 sync --storage-class "DEEP_ARCHIVE" s3://private_assets_bucket s3://private_assets_bucket.backup
