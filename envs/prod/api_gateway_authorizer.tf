resource "aws_api_gateway_authorizer" "archie" {
  name           = "archie"
  type           = "TOKEN"
  rest_api_id    = aws_api_gateway_rest_api.public.id
  authorizer_uri = aws_lambda_function.archie_api_authorizer.invoke_arn
}

resource "aws_lambda_function" "archie_api_authorizer" {
  function_name = "archie_api_authorizer"
  role          = aws_iam_role.archie_api_authorizer.arn
  runtime       = "python3.12"
  handler       = "app.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  environment {
    variables = {
      REGION_NAME           = var.aws_region
      COGNITO_USER_POOL_ID  = aws_cognito_user_pool.archie.id
      COGNITO_APP_CLIENT_ID = aws_cognito_user_pool_client.archie.id
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_iam_role" "archie_api_authorizer" {
  name = "archie_api_authorizer"
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "archie_api_authorizer" {
  role       = aws_iam_role.archie_api_authorizer.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_lambda_permission" "archie_api_authorizer" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.archie_api_authorizer.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = format(
    "arn:aws:execute-api:%s:%s:%s/authorizers/%s",
    var.aws_region,
    var.aws_account,
    aws_api_gateway_rest_api.public.id,
    aws_api_gateway_authorizer.archie.id
  )
}
