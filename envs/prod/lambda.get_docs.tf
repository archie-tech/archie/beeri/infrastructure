resource "aws_lambda_function" "get_docs" {
  function_name = lower("${var.application}_${var.customer}_get_docs")
  role          = aws_iam_role.get_docs.arn
  runtime       = "python3.12"
  handler       = "get_docs.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region        = var.aws_region
      logging_level = "INFO"
      solr_url      = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri/select"
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_lambda_permission" "get_docs" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.get_docs.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = format(
    "arn:aws:execute-api:%s:%s:%s/*/%s%s",
    var.aws_region,
    var.aws_account,
    aws_api_gateway_rest_api.public.id,
    aws_api_gateway_method.get_docs.http_method,
    aws_api_gateway_resource.docs.path
  )
}

resource "aws_iam_role" "get_docs" {
  name = lower("${var.application}_${var.customer}_get_docs")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "get_docs_basic_execution" {
  role       = aws_iam_role.get_docs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "get_docs_eni_management" {
  role       = aws_iam_role.get_docs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}
