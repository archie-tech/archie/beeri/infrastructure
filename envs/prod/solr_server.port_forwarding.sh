#!/bin/bash

# Start port forwarding session from developer work-station to remote host running Solr

export aws_profile=beeri
export instance_id=$(terraform output -raw solr_server_id)

aws ssm start-session \
--region eu-west-1 \
--profile $aws_profile \
--target $instance_id \
--document-name AWS-StartPortForwardingSession \
--parameters "localPortNumber=8983,portNumber=8983"
