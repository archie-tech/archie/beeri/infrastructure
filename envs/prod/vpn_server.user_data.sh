#!/bin/bash -xe

# Redirect output to local log file
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Set time zone
sudo timedatectl set-timezone Asia/Jerusalem

# Download OpenVPN installer script
curl -O https://raw.githubusercontent.com/angristan/openvpn-install/master/openvpn-install.sh
chmod +x openvpn-install.sh