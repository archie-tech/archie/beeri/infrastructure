terraform {
  required_version = ">= 1.0"
  backend "s3" {
    bucket  = "terraform.archie.beer.org.il"
    key     = "terraform.tfstate"
    region  = "eu-west-1"
    profile = "beeri"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.62"
    }
  }
}

provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
  default_tags {
    tags = {
      Application = var.application
      Customer    = var.customer
      Environment = var.environment
      Creator     = "Terraform"
    }
  }
}

provider "aws" {
  alias   = "acm"
  region  = "us-east-1"
  profile = var.aws_profile
}
