resource "aws_lambda_function" "post_dropbox" {
  function_name = lower("${var.application}_${var.customer}_post_dropbox")
  role          = aws_iam_role.post_dropbox.arn
  runtime       = "python3.12"
  timeout       = 30
  handler       = "post_dropbox.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region         = var.aws_region
      secret         = lower("${var.application}_${var.customer}")
      logging_level  = "INFO"
      queue_url      = aws_sqs_queue.ingest_folder.url
      dropbox_bucket = var.private_assets_bucket
      solr_url       = "http://${aws_instance.solr.private_ip}:8983/solr/dropbox"
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_lambda_permission" "post_dropbox" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.post_dropbox.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_region}:${var.aws_account}:${aws_api_gateway_rest_api.public.id}/*/${aws_api_gateway_method.post_dropbox.http_method}${aws_api_gateway_resource.dropbox.path}"
}

resource "aws_iam_role" "post_dropbox" {
  name = lower("${var.application}_${var.customer}_post_dropbox")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "post_dropbox" {
  name = lower("${var.application}_${var.customer}_post_dropbox")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "iam:GetRole",
            "iam:PassRole",
            "secretsmanager:ListSecrets",
            "logs:CreateLogGroup"
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : [
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_post_dropbox",
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_post_dropbox:log-stream:*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:GetQueueAttributes",
            "sqs:GetQueueUrl",
            "sqs:SendMessage",
            "sqs:SendMessageBatch"
          ],
          "Resource" : aws_sqs_queue.ingest_folder.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "secretsmanager:GetResourcePolicy",
            "secretsmanager:GetSecretValue",
            "secretsmanager:DescribeSecret",
            "secretsmanager:ListSecretVersionIds"
          ],
          "Resource" : "arn:aws:secretsmanager:${var.aws_region}:${var.aws_account}:secret:${lower("${var.application}_${var.customer}-??????")}"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "post_dropbox" {
  role       = aws_iam_role.post_dropbox.name
  policy_arn = aws_iam_policy.post_dropbox.arn
}

resource "aws_iam_role_policy_attachment" "post_dropbox_eni_management" {
  role       = aws_iam_role.post_dropbox.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

