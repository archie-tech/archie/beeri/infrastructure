resource "aws_route53_record" "webapp" {
  zone_id = var.hosted_zone_id
  name    = var.webapp_domain_name
  type    = "A"
  alias {
    name                   = aws_cloudfront_distribution.webapp.domain_name
    zone_id                = aws_cloudfront_distribution.webapp.hosted_zone_id #"Z2FDTNDATAQYW2"
    evaluate_target_health = false
  }
}
