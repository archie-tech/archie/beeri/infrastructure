resource "aws_secretsmanager_secret" "archie_beeri" {
  name = lower("${var.application}_${var.customer}")
}

resource "aws_secretsmanager_secret_version" "archie_beeri" {
  secret_id = aws_secretsmanager_secret.archie_beeri.id
  secret_string = jsonencode(
    {
      # archie exports
      "public_assets_bucket" : aws_cloudfront_distribution.public_assets_bucket.domain_name
      "private_assets_bucket" : aws_cloudfront_distribution.private_assets_bucket.domain_name
      "secrets_assets_bucket" : var.secret_assets_bucket
      "docs_endpoint" : var.search_domain_name
      "beeri_ip" : var.beeri_ip
      # aws exports
      "user_pool_id" : aws_cognito_user_pool.archie.id
      "user_pool_web_client_id" : aws_cognito_user_pool_client.archie.id
      "api_endpoint" : var.api_domain_name
    }
  )
}

resource "aws_secretsmanager_secret" "archie_beeri_admin" {
  name = "archie_beeri_admin"
}

resource "aws_secretsmanager_secret_version" "archie_beeri_admin" {
  secret_id = aws_secretsmanager_secret.archie_beeri_admin.id
  secret_string = jsonencode(
    {
      "user_pool_id" : aws_cognito_user_pool.archie.id
      "user_pool_web_client_id" : aws_cognito_user_pool_client.archie.id
      "api_endpoint" : var.api_domain_name
      "docs_endpoint" : var.search_domain_name
      "bucket" : var.admin_webapp_domain_name
    }
  )
}
