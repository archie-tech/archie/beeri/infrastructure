resource "aws_lambda_function" "solr_backup" {
  function_name = lower("${var.application}_${var.customer}_solr_backup")
  role          = aws_iam_role.solr_backup.arn
  runtime       = "python3.12"
  handler       = "solr_backup.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  timeout       = 60
  memory_size   = 1024
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      logging_level             = "INFO"
      solr_url                  = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri"
      archie_base64_credentials = var.archie_base64_credentials
      backup_bucket             = var.solr_backup_bucket
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_iam_role" "solr_backup" {
  name = lower("${var.application}_${var.customer}_solr_backup")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "solr_backup_basic_execution" {
  role       = aws_iam_role.solr_backup.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "solr_backup_eni_management" {
  role       = aws_iam_role.solr_backup.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_iam_policy" "solr_backup" {
  name = lower("${var.application}_${var.customer}_solr_backup")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" = "Allow",
          "Action" : "s3:ListBucket",
          "Resource" : data.aws_s3_bucket.solr_backup.arn
        },
        {
          "Effect" = "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject"
          ],
          "Resource" : "${data.aws_s3_bucket.solr_backup.arn}/*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "solr_backup" {
  role       = aws_iam_role.solr_backup.name
  policy_arn = aws_iam_policy.solr_backup.arn
}

resource "aws_cloudwatch_event_rule" "solr_backup" {
  name                = lower("${var.application}_${var.customer}_solr_backup")
  description         = "Run backup trigger every day at 01:00 (22:00 UTC)"
  schedule_expression = "cron(0 22 * * ? *)"
  tags = {
    Name = lower("${var.application}_${var.customer}_solr_backup")
  }
}

resource "aws_cloudwatch_event_target" "solr_backup" {
  rule  = aws_cloudwatch_event_rule.solr_backup.name
  arn   = aws_lambda_function.solr_backup.arn
  input = jsonencode({ "trigger" : "events-bridge" })
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_solr_backup" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.solr_backup.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.solr_backup.arn
}

data "aws_s3_bucket" "solr_backup" {
  provider = aws.acm
  bucket   = var.solr_backup_bucket
}

resource "aws_s3_bucket_versioning" "solr_backup" {
  provider = aws.acm
  bucket   = data.aws_s3_bucket.solr_backup.id
  versioning_configuration {
    status = "Enabled"
  }
}
