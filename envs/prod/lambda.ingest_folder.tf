resource "aws_lambda_function" "ingest_folder" {
  function_name = lower("${var.application}_${var.customer}_ingest_folder")
  role          = aws_iam_role.ingest_folder.arn
  runtime       = "python3.12"
  timeout       = 30
  handler       = "ingest_folder.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region         = var.aws_region
      secret         = lower("${var.application}_${var.customer}")
      logging_level  = "INFO"
      ingest_bucket  = aws_s3_bucket.ingest.id
      dynamodb_table = aws_dynamodb_table.reports.id
      queue_url      = aws_sqs_queue.ingest_folder.url
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_lambda_permission" "ingest_folder" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ingest_folder.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn    = "arn:aws:execute-api:${var.aws_region}:${var.aws_account}:${aws_api_gateway_rest_api.public.id}/*/${aws_api_gateway_method.put_docs.http_method}${aws_api_gateway_resource.docs.path}"
}

resource "aws_iam_role" "ingest_folder" {
  name = lower("${var.application}_${var.customer}_ingest_folder")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "ingest_folder" {
  name = lower("${var.application}_${var.customer}_ingest_folder")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "iam:GetRole",
            "iam:PassRole",
            "secretsmanager:ListSecrets",
            "dynamodb:ListTables",
            "logs:CreateLogGroup"
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "logs:CreateLogStream",
            "logs:PutLogEvents"
          ],
          "Resource" : [
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_ingest_folder",
            "arn:aws:logs:${var.aws_region}:${var.aws_account}:log-group:/aws/lambda/${lower("${var.application}_${var.customer}")}_ingest_folder:log-stream:*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:GetQueueAttributes",
            "sqs:GetQueueUrl",
            "sqs:SendMessage",
            "sqs:SendMessageBatch"
          ],
          "Resource" : aws_sqs_queue.ingest_folder.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:List*",
            "s3:Get*"
          ],
          "Resource" : aws_s3_bucket.ingest.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "dynamodb:PutItem"
          ],
          "Resource" : aws_dynamodb_table.reports.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "secretsmanager:GetResourcePolicy",
            "secretsmanager:GetSecretValue",
            "secretsmanager:DescribeSecret",
            "secretsmanager:ListSecretVersionIds"
          ],
          "Resource" : "arn:aws:secretsmanager:${var.aws_region}:${var.aws_account}:secret:${lower("${var.application}_${var.customer}-??????")}"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "ecs:RunTask"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_folder" {
  role       = aws_iam_role.ingest_folder.name
  policy_arn = aws_iam_policy.ingest_folder.arn
}

resource "aws_iam_role_policy_attachment" "ingest_folder_eni_management" {
  role       = aws_iam_role.ingest_folder.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_s3_bucket" "ingest" {
  bucket = var.ingest_folder_bucket
}

resource "aws_sqs_queue" "ingest_folder" {
  name = lower("${var.application}_${var.customer}_ingest_folder")
  redrive_policy = jsonencode({
    "deadLetterTargetArn" = aws_sqs_queue.ingest_folder_dlq.arn,
    "maxReceiveCount"     = 1
  })
  tags = {
    Name = lower("${var.application}_${var.customer}_ingest_folder")
  }
}

resource "aws_sqs_queue" "ingest_folder_dlq" {
  name = lower("${var.application}_${var.customer}_ingest_folder_dlq")
  tags = {
    Name        = lower("${var.application}_${var.customer}_ingest_folder_dlq")
    Description = "Dead Letter Queue for ingest_folder queue"
  }
}
