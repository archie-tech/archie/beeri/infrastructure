resource "aws_instance" "vpn" {
  ami                         = var.vpn_ami
  instance_type               = var.vpn_instance_type
  key_name                    = var.ec2_key_pair
  associate_public_ip_address = true
  subnet_id                   = var.vpn_subnet_id
  vpc_security_group_ids      = [aws_security_group.vpn.id]
  iam_instance_profile        = aws_iam_instance_profile.vpn.name
  user_data_replace_on_change = true
  user_data = templatefile(
    "${path.module}/vpn_server.user_data.sh", {}
  )
  tags = {
    Name = "${var.application} ${var.customer} OpenVpn"
  }
  lifecycle {
    create_before_destroy = true
    ignore_changes = [
      user_data
    ]
  }
}

resource "aws_eip" "vpn" {
  instance = aws_instance.vpn.id
  domain   = "vpc"
  tags = {
    Name = "${var.application} ${var.customer} OpenVpn"
  }
}

resource "aws_route53_record" "vpn" {
  zone_id = var.hosted_zone_id
  name    = var.vpn_domain_name
  type    = "A"
  ttl     = 300
  records = [aws_eip.vpn.public_ip]
}

resource "aws_iam_role" "vpn" {
  name = lower("${var.application}_${var.customer}_OpenVpn")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow",
        }
      ]
    }
  )
}

resource "aws_iam_instance_profile" "vpn" {
  name = lower("${var.application}_${var.customer}_OpenVpn")
  role = aws_iam_role.vpn.name
}


resource "aws_iam_role_policy_attachment" "vpn_ssm" {
  role       = aws_iam_role.vpn.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_security_group" "vpn" {
  name   = lower("${var.application}_${var.customer}_OpenVpn")
  vpc_id = var.vpn_vpc_id
  tags = {
    Name = "${var.application} ${var.customer} OpenVpn"
  }
  ingress {
    description = "Allow SSH from anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "Allow OpenVpn custome UDP service from anywhere"
    from_port   = 1194
    to_port     = 1194
    protocol    = "udp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "vpn_server_id" {
  value = aws_instance.vpn.id
}
