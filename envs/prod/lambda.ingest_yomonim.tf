resource "aws_lambda_function" "ingest_yomonim" {
  function_name = lower("${var.application}_${var.customer}_ingest_yomonim")
  role          = aws_iam_role.ingest_yomonim.arn
  runtime       = "python3.12"
  handler       = "ingest_yomonim.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  timeout       = 300
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region                    = var.aws_region
      environment               = var.environment
      secret                    = lower("${var.application}_${var.customer}")
      logging_level             = "INFO"
      ingest_bucket             = aws_s3_bucket.ingest.id
      dynamodb_table            = aws_dynamodb_table.reports.id
      queue_url                 = aws_sqs_queue.ingest_folder.url
      public_assets_bucket      = var.public_assets_bucket
      private_assets_bucket     = var.private_assets_bucket
      secret_assets_bucket      = var.secret_assets_bucket
      solr_url                  = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri"
      archie_base64_credentials = var.archie_base64_credentials
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_iam_role" "ingest_yomonim" {
  name = lower("${var.application}_${var.customer}_ingest_yomonim")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_yomonim_basic_execution" {
  role       = aws_iam_role.ingest_yomonim.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "ingest_yomonim_eni_management" {
  role       = aws_iam_role.ingest_yomonim.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_iam_policy" "ingest_yomonim" {
  name = lower("${var.application}_${var.customer}_ingest_yomonim")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:List*",
            "s3:Get*",
            "s3:Put*"
          ],
          "Resource" : [
            aws_s3_bucket.ingest.arn,
            "${aws_s3_bucket.ingest.arn}/*",
            data.aws_s3_bucket.yomonim.arn,
            "${data.aws_s3_bucket.yomonim.arn}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : "lambda:InvokeFunction",
          "Resource" : "${aws_lambda_function.ingest_folder.arn}*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_yomonim" {
  role       = aws_iam_role.ingest_yomonim.name
  policy_arn = aws_iam_policy.ingest_yomonim.arn
}

resource "aws_iam_role_policy_attachment" "yomonim_ingest_file" {
  role       = aws_iam_role.ingest_yomonim.name
  policy_arn = aws_iam_policy.ingest_file.arn
}

data "aws_s3_bucket" "yomonim" {
  bucket = var.ingest_yomonim_bucket
}

resource "aws_s3_bucket_notification" "ingest_yomonim" {
  bucket = data.aws_s3_bucket.yomonim.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.ingest_yomonim.arn
    events              = ["s3:ObjectCreated:*"]
  }
  depends_on = [aws_lambda_permission.ingest_yomonim]
}

resource "aws_lambda_permission" "ingest_yomonim" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ingest_yomonim.arn
  principal     = "s3.amazonaws.com"
  source_arn    = data.aws_s3_bucket.yomonim.arn
}
