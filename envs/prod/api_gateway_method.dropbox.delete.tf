resource "aws_api_gateway_method" "delete_dropbox" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.dropbox.id
  http_method   = "DELETE"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
  request_parameters = {
    "method.request.querystring.docId"          = true,
    "method.request.querystring.dcFormat"       = true
  }
}

resource "aws_api_gateway_method_response" "delete_dropbox" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.dropbox.id
  http_method = aws_api_gateway_method.delete_dropbox.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.delete_dropbox]
}

resource "aws_api_gateway_integration" "delete_dropbox" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.dropbox.id
  http_method             = aws_api_gateway_method.delete_dropbox.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.delete_dropbox.invoke_arn
  request_templates = {
    "application/json" = jsonencode({
      "username" : "$context.authorizer.username",
      "docId" : "$input.params('docId')",
      "dcFormat" : "$input.params('dcFormat')"
    })
  }
}

resource "aws_api_gateway_integration_response" "delete_dropbox" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.dropbox.id
  http_method = aws_api_gateway_method.delete_dropbox.http_method
  status_code = aws_api_gateway_method_response.delete_dropbox.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.delete_dropbox]
}
