variable "application" { default = "Archie" }
variable "customer" { default = "Beeri" }
variable "environment" { default = "Test" }
variable "region" { default = "eu-west-2" }
variable "vpc_cidr" { default = "10.0.0.0/16" }
variable "public_subnet_cidr" { default = "10.0.1.0/24" }
variable "private_subnet_cidr" { default = "10.0.2.0/24" }
