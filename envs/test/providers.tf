terraform {
  required_version = ">= 1.0"
  backend "s3" {
    bucket  = "terraform.archie-tech.com"
    key     = "archie/beeri/test/terraform.tfstate"
    region  = "eu-central-1"
    profile = "archie-tech"
  }
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.50"
    }
  }
}

provider "aws" {
  region                  = var.region
  shared_credentials_file = "%USERPROFILE%/.aws/credentials"
  profile                 = "archie-tech-test"
  default_tags {
    tags = {
      Application = var.application
      Customer    = var.customer
      Environment = var.environment
      Creator     = "Terraform"
    }
  }
}
