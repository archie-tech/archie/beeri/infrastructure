#!/bin/bash

# Get VNC passowrd from file /var/opt/vnc_password.txt

# Config
export instance_id=$(terraform output -raw work_station_id)
export aws_profile="arinamal-dev"
export aws_region='eu-west-1'


# Start VNC server on the remote instance
aws --profile=$aws_profile --region=$aws_region ssm send-command --document-name "AWS-RunShellScript" --parameters 'commands=["runuser -u ubuntu -- vncserver -localhost no :1"]' --targets "Key=instanceids,Values=$instance_id"

# Strart session from local PC to remote instance
aws --profile=$aws_profile --region=$aws_region ssm start-session --document-name AWS-StartPortForwardingSession --parameters "localPortNumber=5901,portNumber=5901" --target "$instance_id"
