resource "aws_api_gateway_method" "delete_docs" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.docs.id
  http_method   = "DELETE"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
  request_parameters = {
    "method.request.querystring.dcAccessRights" = true,
    "method.request.querystring.docId"          = true,
    "method.request.querystring.dcFormat"       = true
  }
}

resource "aws_api_gateway_method_response" "delete_docs" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.docs.id
  http_method = aws_api_gateway_method.delete_docs.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.delete_docs]
}

resource "aws_api_gateway_integration" "delete_docs" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.docs.id
  http_method             = aws_api_gateway_method.delete_docs.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.delete_docs.invoke_arn
  request_templates = {
    "application/json" = jsonencode({
      "username" : "$context.authorizer.username",
      "dcAccessRights" : "$input.params('dcAccessRights')",
      "docId" : "$input.params('docId')",
      "dcFormat" : "$input.params('dcFormat')"
    })
  }
}

resource "aws_api_gateway_integration_response" "delete_docs" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.docs.id
  http_method = aws_api_gateway_method.delete_docs.http_method
  status_code = aws_api_gateway_method_response.delete_docs.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.delete_docs]
}
