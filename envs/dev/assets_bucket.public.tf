# Object Ownership: ACLs disabled (Bucket owner enforced)
# Block all public access: disabled
data "aws_s3_bucket" "public_assets" {
  bucket = var.public_assets_bucket
}

resource "aws_s3_bucket_versioning" "public_assets" {
  bucket = data.aws_s3_bucket.public_assets.id
  versioning_configuration {
    status = "Enabled"
  }
}

data "aws_s3_bucket" "public_assets_backup" {
  provider = aws.acm
  bucket   = "${var.public_assets_bucket}.backup"
}

resource "aws_s3_bucket_versioning" "public_assets_backup" {
  provider = aws.acm
  bucket   = data.aws_s3_bucket.public_assets_backup.id
  versioning_configuration {
    status = "Enabled"
  }
}


resource "aws_cloudfront_distribution" "public_assets_bucket" {
  enabled     = true
  price_class = "PriceClass_200"
  origin {
    domain_name = data.aws_s3_bucket.public_assets.bucket_regional_domain_name
    origin_id   = data.aws_s3_bucket.public_assets.bucket_regional_domain_name
    s3_origin_config {
      #origin_access_identity = "origin-access-identity/cloudfront/EKP61VV67EFBP"
      origin_access_identity = aws_cloudfront_origin_access_identity.archie_beeri_public_assets.cloudfront_access_identity_path
    }
  }
  default_cache_behavior {
    compress               = true
    target_origin_id       = data.aws_s3_bucket.public_assets.bucket_regional_domain_name
    viewer_protocol_policy = "https-only"
    min_ttl                = 0
    default_ttl            = 0
    max_ttl                = 0
    allowed_methods        = ["GET", "HEAD"] # ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods         = ["GET", "HEAD"]
    cache_policy_id        = "658327ea-f89d-4fab-a63d-7e88639e58f6"
    #origin_request_policy_id = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  }
  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }
  viewer_certificate {
    cloudfront_default_certificate = true
  }
}

resource "aws_cloudfront_origin_access_identity" "archie_beeri_public_assets" {
  comment = "access-identity-archie-beeri-public.s3.eu-west-1.amazonaws.com"
}

resource "aws_s3_bucket_policy" "archie_beeri_public_assets" {
  bucket = data.aws_s3_bucket.public_assets.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "PublicReadGetObject",
          "Effect" : "Allow",
          "Principal" : "*",
          "Action" : "s3:GetObject",
          "Resource" : "${data.aws_s3_bucket.public_assets.arn}/*"
        }
      ]
    }
  )
}

resource "aws_s3_bucket_cors_configuration" "archie_beeri_public_assets" {
  bucket = data.aws_s3_bucket.public_assets.id
  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = [
      "GET",
      "PUT",
      "POST",
      "DELETE",
      "HEAD"
    ]
    allowed_origins = ["https://${var.webapp_domain_name}"]
    expose_headers  = []
  }
}

resource "aws_iam_role" "public_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_public_assets_bucket_replication")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "s3.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "public_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_public_assets_bucket_replication")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetReplicationConfiguration",
            "s3:ListBucket",
          ]
          "Resource" : [data.aws_s3_bucket.public_assets.arn]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetObjectVersionForReplication",
            "s3:GetObjectVersionAcl",
            "s3:GetObjectVersionTagging",
          ]
          "Resource" : ["${data.aws_s3_bucket.public_assets.arn}/*"]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:ReplicateObject",
            #"s3:ReplicateDelete",
            "s3:ReplicateTags"
          ]
          "Resource" : ["${data.aws_s3_bucket.public_assets_backup.arn}/*"]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "public_assets_bucket_replication" {
  role       = aws_iam_role.public_assets_bucket_replication.name
  policy_arn = aws_iam_policy.public_assets_bucket_replication.arn
}

resource "aws_s3_bucket_replication_configuration" "public_assets" {
  depends_on = [
    aws_s3_bucket_versioning.public_assets,
    aws_s3_bucket_versioning.public_assets_backup
  ]
  role   = aws_iam_role.public_assets_bucket_replication.arn
  bucket = data.aws_s3_bucket.public_assets.id
  rule {
    id     = "public-1"
    status = var.bucket_replication_status
    destination {
      bucket        = data.aws_s3_bucket.public_assets_backup.arn
      storage_class = "DEEP_ARCHIVE"
    }
  }
}

# replicate existing objects
# aws s3 sync --storage-class "DEEP_ARCHIVE" s3://public_assets_bucket s3://public_assets_bucket.backup
