resource "aws_api_gateway_method" "put_docs" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.docs.id
  http_method   = "PUT"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
}

resource "aws_api_gateway_method_response" "put_docs" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.docs.id
  http_method = aws_api_gateway_method.put_docs.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.put_docs]
}

resource "aws_api_gateway_integration" "put_docs" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.docs.id
  http_method             = aws_api_gateway_method.put_docs.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.ingest_folder.invoke_arn
  request_templates = {
    "application/json" = <<EOF
{
  "username":"$context.authorizer.username",
  "user_input" : $input.json('$.user_input')  
}
    EOF
  }
}

resource "aws_api_gateway_integration_response" "put_docs" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.docs.id
  http_method = aws_api_gateway_method.put_docs.http_method
  status_code = aws_api_gateway_method_response.put_docs.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.put_docs]
}
