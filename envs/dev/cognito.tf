resource "aws_cognito_user_pool" "archie" {
  name = lower("${var.application}_${var.customer}")
  account_recovery_setting {
    recovery_mechanism {
      name     = "verified_email"
      priority = 1
    }
  }
  tags = {
    Name = lower("${var.application}_${var.customer}")
  }
}

resource "aws_cognito_user_pool_client" "archie" {
  name                   = lower("${var.application}_${var.customer}")
  user_pool_id           = aws_cognito_user_pool.archie.id
  access_token_validity  = 60
  id_token_validity      = 60
  refresh_token_validity = 30
  token_validity_units {
    access_token  = "minutes"
    id_token      = "minutes"
    refresh_token = "days"
  }
}

resource "aws_cognito_user_group" "archie_managers" {
  name         = "archie_managers"
  user_pool_id = aws_cognito_user_pool.archie.id
}

resource "aws_cognito_user_group" "archie_editors" {
  name         = "archie_editors"
  user_pool_id = aws_cognito_user_pool.archie.id
}
