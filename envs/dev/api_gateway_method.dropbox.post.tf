resource "aws_api_gateway_method" "post_dropbox" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.dropbox.id
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
}

resource "aws_api_gateway_method_response" "post_dropbox" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.dropbox.id
  http_method = aws_api_gateway_method.post_dropbox.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.post_dropbox]
}

resource "aws_api_gateway_integration" "post_dropbox" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.dropbox.id
  http_method             = aws_api_gateway_method.post_dropbox.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.post_dropbox.invoke_arn
  request_templates = {
    "application/json" = <<EOF
{
  "username":"$context.authorizer.username",
  "body" : $input.json('$')
}
    EOF
  }
}

resource "aws_api_gateway_integration_response" "post_dropbox" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.dropbox.id
  http_method = aws_api_gateway_method.post_dropbox.http_method
  status_code = aws_api_gateway_method_response.post_dropbox.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.post_dropbox]
}
