resource "aws_lambda_function" "delete_docs" {
  function_name = lower("${var.application}_${var.customer}_delete_docs")
  role          = aws_iam_role.delete_docs.arn
  runtime       = "python3.12"
  handler       = "delete.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region                    = var.aws_region
      logging_level             = "INFO"
      solr_url                  = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri"
      archie_base64_credentials = var.archie_base64_credentials
      public_assets_bucket      = var.public_assets_bucket
      private_assets_bucket     = var.private_assets_bucket
      secret_assets_bucket      = var.secret_assets_bucket
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_lambda_permission" "delete_docs" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.delete_docs.function_name
  principal     = "apigateway.amazonaws.com"
  # arn:aws:execute-api:eu-west-1:946161444602:9gw7otwab3/*/PUT/docs
  source_arn = format(
    "arn:aws:execute-api:%s:%s:%s/*/%s%s",
    var.aws_region,
    var.aws_account,
    aws_api_gateway_rest_api.public.id,
    aws_api_gateway_method.delete_docs.http_method,
    aws_api_gateway_resource.docs.path
  )
}

resource "aws_iam_role" "delete_docs" {
  name = lower("${var.application}_${var.customer}_delete_docs")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "delete_docs_basic_execution" {
  role       = aws_iam_role.delete_docs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "delete_docs_eni_management" {
  role       = aws_iam_role.delete_docs.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_iam_policy" "delete_docs" {
  name = lower("${var.application}_${var.customer}_delete_docs")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : "s3:DeleteObject",
          "Resource" : [
            "arn:aws:s3:::${var.public_assets_bucket}/*",
            "arn:aws:s3:::${var.private_assets_bucket}/*",
            "arn:aws:s3:::${var.secret_assets_bucket}/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "delete_docs" {
  role       = aws_iam_role.delete_docs.name
  policy_arn = aws_iam_policy.delete_docs.arn
}
