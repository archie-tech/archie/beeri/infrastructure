# AWS console > IAM > Users > gitlab > Security credentials > Create access key
# GitLab project settings > CI/CD > Variables: AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_DEFAULT_REGION

resource "aws_iam_user" "gitlab" {
  name = "gitlab"
  tags = {
    Name = "${var.application} ${var.customer} GitLab"
  }
}

resource "aws_iam_user_policy" "gitlab" {
  name = lower("${var.application}_${var.customer}_gitlab")
  user = aws_iam_user.gitlab.name
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : [
            "s3:GetObject",
            "s3:PutObject",
            "s3:DeleteObject",
            "s3:ListBucket"
          ],
          "Effect" : "Allow",
          "Resource" : [
            aws_s3_bucket.webapp.arn,
            "${aws_s3_bucket.webapp.arn}/*",
            aws_s3_bucket.admin_webapp.arn,
            "${aws_s3_bucket.admin_webapp.arn}/*",
            "arn:aws:s3:::${var.deployments_bucket}",
            "arn:aws:s3:::${var.deployments_bucket}/*"
          ]
        },
        {
          "Action" : "lambda:UpdateFunctionCode",
          "Effect" : "Allow",
          "Resource" : "arn:aws:lambda:${var.aws_region}:${var.aws_account}:function:*"
        },
        {
          "Action" : [
            "secretsmanager:GetResourcePolicy",
            "secretsmanager:GetSecretValue",
            "secretsmanager:DescribeSecret",
            "secretsmanager:ListSecretVersionIds"
          ],
          "Effect" : "Allow",
          "Resource" : [
            "arn:aws:secretsmanager:${var.aws_region}:${var.aws_account}:secret:archie_beeri-*",
            "arn:aws:secretsmanager:${var.aws_region}:${var.aws_account}:secret:archie_beeri_admin-*"
          ]
        },
        {
          "Sid" : "GetAuthorizationToken",
          "Action" : ["ecr:GetAuthorizationToken"],
          "Resource" : "*",
          "Effect" : "Allow",
        },
        {
          "Sid" : "EcrPush",
          "Action" : [
            "ecr:BatchGetImage",
            "ecr:BatchCheckLayerAvailability",
            "ecr:CompleteLayerUpload",
            "ecr:GetDownloadUrlForLayer",
            "ecr:InitiateLayerUpload",
            "ecr:PutImage",
            "ecr:UploadLayerPart",
          ],
          "Resource" : "arn:aws:ecr:*:${var.aws_account}:repository/*",
          "Effect" : "Allow"
        }
      ]
    }
  )
}
