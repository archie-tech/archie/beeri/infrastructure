resource "aws_lambda_function" "multimedia_utilities" {
  function_name = lower("${var.application}_${var.customer}_multimedia_utilities")
  role          = aws_iam_role.multimedia_utilities.arn
  image_uri     = "${aws_ecr_repository.multimedia_utilities.repository_url}:latest"
  package_type  = "Image"
  memory_size   = 4096
  timeout       = 60
  ephemeral_storage {
    size = 10240 # Min 512 MB and the Max 10240 MB
  }
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      logging_level         = "INFO"
      ingest_bucket         = aws_s3_bucket.ingest.id
      public_assets_bucket  = var.public_assets_bucket
      private_assets_bucket = var.private_assets_bucket
      secret_assets_bucket  = var.secret_assets_bucket
    }
  }
  lifecycle {
    ignore_changes = [image_uri]
  }
}

resource "aws_iam_role" "multimedia_utilities" {
  name = lower("${var.application}_${var.customer}_multimedia_utilities")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "multimedia_utilities" {
  name = lower("${var.application}_${var.customer}_multimedia_utilities")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject"
          ],
          "Resource" : [
            "${aws_s3_bucket.ingest.arn}/*",
            "${data.aws_s3_bucket.public_assets.arn}/*",
            "${data.aws_s3_bucket.private_assets.arn}/*",
            "${data.aws_s3_bucket.secret_assets.arn}/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "multimedia_utilities" {
  role       = aws_iam_role.multimedia_utilities.name
  policy_arn = aws_iam_policy.multimedia_utilities.arn
}

resource "aws_iam_role_policy_attachment" "multimedia_utilities_basic_execution" {
  role       = aws_iam_role.multimedia_utilities.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "multimedia_utilities_eni_management" {
  role       = aws_iam_role.multimedia_utilities.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_ecr_repository" "multimedia_utilities" {
  name                 = lower("${var.application}_${var.customer}_multimedia_utilities")
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}

resource "aws_cloudwatch_event_rule" "multimedia_utilities" {
  name        = lower("${var.application}_${var.customer}_multimedia_utilities")
  description = "Invoke multimedia_utilities function asynchronously"
  event_pattern = jsonencode(
    {
      "source" : ["archie_beeri"],
      "detail-type" : ["multimedia_utilities_task"]
  })
  tags = {
    Name = lower("${var.application}_${var.customer}_multimedia_utilities")
  }
}

resource "aws_cloudwatch_event_target" "multimedia_utilities" {
  rule      = aws_cloudwatch_event_rule.multimedia_utilities.name
  target_id = "multimedia_utilities"
  arn       = aws_lambda_function.multimedia_utilities.arn
}

resource "aws_lambda_permission" "multimedia_utilities" {
  statement_id  = "AllowExecutionFromEventsBridge"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.multimedia_utilities.arn
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.multimedia_utilities.arn
}
