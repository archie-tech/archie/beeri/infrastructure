resource "aws_lambda_function" "ingest_dropbox" {
  function_name = lower("${var.application}_${var.customer}_ingest_dropbox")
  role          = aws_iam_role.ingest_dropbox.arn
  image_uri     = "${aws_ecr_repository.ingest_dropbox.repository_url}:latest"
  package_type  = "Image"
  memory_size   = 4096
  timeout       = 300
  ephemeral_storage {
    size = 10240 # Min 512 MB and the Max 10240 MB
  }
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region        = var.aws_region
      environment   = var.environment
      secret        = lower("${var.application}_${var.customer}")
      logging_level = "INFO"
      ingest_bucket = var.private_assets_bucket
      solr_url      = "http://${aws_instance.solr.private_ip}:8983/solr/dropbox"
    }
  }
  lifecycle {
    ignore_changes = [image_uri]
  }
}

resource "aws_ecr_repository" "ingest_dropbox" {
  name                 = lower("${var.application}_${var.customer}_ingest_dropbox")
  image_tag_mutability = "MUTABLE"
  image_scanning_configuration {
    scan_on_push = true
  }
}


resource "aws_iam_role" "ingest_dropbox" {
  name = lower("${var.application}_${var.customer}_ingest_dropbox")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_dropbox_basic_execution" {
  role       = aws_iam_role.ingest_dropbox.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "ingest_dropbox_eni_management" {
  role       = aws_iam_role.ingest_dropbox.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}

resource "aws_iam_policy" "ingest_dropbox" {
  name = lower("${var.application}_${var.customer}_ingest_dropbox")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:List*",
            "s3:Get*"
          ],
          "Resource" : [
            "arn:aws:s3:::${var.dropbox_bucket}",
            "arn:aws:s3:::${var.dropbox_bucket}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:Put*"
          ],
          "Resource" : [
            "arn:aws:s3:::${var.private_assets_bucket}",
            "arn:aws:s3:::${var.private_assets_bucket}/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_dropbox" {
  role       = aws_iam_role.ingest_dropbox.name
  policy_arn = aws_iam_policy.ingest_dropbox.arn
}

resource "aws_s3_bucket_notification" "ingest_dropbox" {
  bucket = var.dropbox_bucket
  lambda_function {
    lambda_function_arn = aws_lambda_function.ingest_dropbox.arn
    events              = ["s3:ObjectCreated:*"]
  }
  depends_on = [aws_lambda_permission.ingest_dropbox]
}

resource "aws_lambda_permission" "ingest_dropbox" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.ingest_dropbox.arn
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${var.dropbox_bucket}"
}
