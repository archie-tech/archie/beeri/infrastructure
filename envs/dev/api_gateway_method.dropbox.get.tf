resource "aws_api_gateway_method" "get_dropbox" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.dropbox.id
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
}

resource "aws_api_gateway_integration" "get_dropbox" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.dropbox.id
  http_method             = aws_api_gateway_method.get_dropbox.http_method
  integration_http_method = "POST"
  type                    = "AWS_PROXY"
  uri                     = aws_lambda_function.get_dropbox.invoke_arn
}
