resource "aws_api_gateway_method" "folders_options" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.folders.id
  http_method   = "OPTIONS"
  authorization = "NONE"
}

resource "aws_api_gateway_method_response" "folders_options" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.folders.id
  http_method = aws_api_gateway_method.folders_options.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = true,
    "method.response.header.Access-Control-Allow-Methods" = true,
    "method.response.header.Access-Control-Allow-Origin"  = true
  }
  depends_on = [aws_api_gateway_method.folders_options]
}

resource "aws_api_gateway_integration" "folders_options" {
  rest_api_id          = aws_api_gateway_rest_api.public.id
  resource_id          = aws_api_gateway_resource.folders.id
  http_method          = aws_api_gateway_method.folders_options.http_method
  type                 = "MOCK"
  passthrough_behavior = "WHEN_NO_MATCH"
  request_templates = {
    "application/json" = jsonencode({ statusCode = 200 })
  }
  depends_on = [aws_api_gateway_method.folders_options]
}

resource "aws_api_gateway_integration_response" "folders_options" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.folders.id
  http_method = aws_api_gateway_method.folders_options.http_method
  status_code = aws_api_gateway_method_response.folders_options.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Headers" = "'Content-Type,X-Amz-Date,Authorization,X-Api-Key,X-Amz-Security-Token'",
    "method.response.header.Access-Control-Allow-Methods" = "'OPTIONS,POST,GET'",
    "method.response.header.Access-Control-Allow-Origin"  = "'*'"
  }
  depends_on = [aws_api_gateway_method_response.folders_options]
}
