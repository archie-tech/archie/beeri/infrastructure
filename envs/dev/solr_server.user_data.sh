#!/bin/bash -xe

# Redirect output to local log file
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

# Install packages
dnf -y install amazon-cloudwatch-agent docker

# Install Solr
systemctl start docker
systemctl enable docker
docker run -d --restart=unless-stopped -p 8983:8983 --name solr solr
#mkdir /var/solr
#chown 8983 /var/solr
#docker run -d -v "/var/solr:/var/solr" --restart=unless-stopped -p 8983:8983 --name solr solr

# Configure Solr
aws s3 sync s3://${deployments_bucket}/archie_beeri_solr/setup/ /var/opt/setup/
bash /var/opt/setup/scripts/configure_solr.sh
aws s3 cp s3://${solr_backup_bucket}/data.json /var/opt/
bash /var/opt/setup/scripts/import_data.sh

# Configure CloudWatch agent
cat > /var/opt/cloudwatch-agent-config.json <<EOF
{
    "agent": {
        "run_as_user": "root"
    },
    "metrics": {
        "aggregation_dimensions": [
            [
                "InstanceId"
            ]
        ],
        "append_dimensions": {
            "InstanceId": "\$${aws:InstanceId}"
        },
        "metrics_collected": {
            "mem": {
                "measurement": [
                    "used", "used_percent"
                ],
                "metrics_collection_interval": 300
            },
            "disk": {
                "measurement": [
                    "disk_used",
                    "disk_used_percent"
                ],
                "metrics_collection_interval": 300,
                "resources": [
                    "/"
                ]
            }
        }
    }
}
EOF

/opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/var/opt/cloudwatch-agent-config.json
