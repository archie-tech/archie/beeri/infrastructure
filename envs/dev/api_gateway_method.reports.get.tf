resource "aws_api_gateway_method" "get_reports" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.reports.id
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
  request_parameters = {
    "method.request.querystring.partition_key" = true
    "method.request.querystring.report_type"   = true
  }
}

resource "aws_api_gateway_method_response" "get_reports" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.reports.id
  http_method = aws_api_gateway_method.get_reports.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.get_reports]
}

resource "aws_api_gateway_integration" "get_reports" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.reports.id
  http_method             = aws_api_gateway_method.get_reports.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.get_reports.invoke_arn
  request_templates = {
    "application/json" = jsonencode(
      {
        partition_key = "$input.params('partition_key')"
        report_type   = "$input.params('report_type')"
        username      = "$context.authorizer.username"
      }
    )
  }
}

resource "aws_api_gateway_integration_response" "get_reports" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.reports.id
  http_method = aws_api_gateway_method.get_reports.http_method
  status_code = aws_api_gateway_method_response.get_reports.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.get_reports]
}
