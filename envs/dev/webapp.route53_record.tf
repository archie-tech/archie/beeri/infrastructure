resource "aws_route53_record" "webapp" {
  zone_id = var.hosted_zone_id
  name    = var.webapp_domain_name
  type    = "CNAME"
  ttl     = 60
  records = [aws_cloudfront_distribution.webapp.domain_name]
}
