resource "aws_lambda_function" "cleanup" {
  function_name = lower("${var.application}_${var.customer}_cleanup")
  role          = aws_iam_role.cleanup.arn
  runtime       = "python3.12"
  handler       = "cleanup.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  timeout       = 60
  memory_size   = 1024
  environment {
    variables = {
      logging_level = "INFO"
      ingest_bucket = aws_s3_bucket.ingest.id
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_iam_role" "cleanup" {
  name = lower("${var.application}_${var.customer}_cleanup")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "cleanup_basic_execution" {
  role       = aws_iam_role.cleanup.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_policy" "cleanup" {
  name = lower("${var.application}_${var.customer}_cleanup")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" = "Allow",
          "Action" : "s3:ListBucket",
          "Resource" : aws_s3_bucket.ingest.arn
        },
        {
          "Effect" = "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject",
            "s3:DeleteObject"
          ],
          "Resource" : "${aws_s3_bucket.ingest.arn}/*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "cleanup" {
  role       = aws_iam_role.cleanup.name
  policy_arn = aws_iam_policy.cleanup.arn
}

resource "aws_cloudwatch_event_rule" "cleanup" {
  name                = lower("${var.application}_${var.customer}_cleanup")
  description         = "Run cleanup trigger every day at 01:00 (22:00 UTC)"
  schedule_expression = "cron(0 22 * * ? *)"
  tags = {
    Name = lower("${var.application}_${var.customer}_cleanup")
  }
}

resource "aws_cloudwatch_event_target" "cleanup" {
  rule  = aws_cloudwatch_event_rule.cleanup.name
  arn   = aws_lambda_function.cleanup.arn
  input = jsonencode({ "trigger" : "events-bridge" })
}

resource "aws_lambda_permission" "allow_cloudwatch_to_call_cleanup" {
  statement_id  = "AllowExecutionFromCloudWatch"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.cleanup.function_name
  principal     = "events.amazonaws.com"
  source_arn    = aws_cloudwatch_event_rule.cleanup.arn
}
