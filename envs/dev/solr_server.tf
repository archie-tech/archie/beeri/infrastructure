resource "aws_instance" "solr" {
  ami           = var.solr_ami
  instance_type = var.solr_instance_type
  root_block_device {
    volume_size = 12
  }
  key_name                    = var.ec2_key_pair
  subnet_id                   = aws_subnet.private.id
  vpc_security_group_ids      = [aws_security_group.solr.id]
  iam_instance_profile        = aws_iam_instance_profile.solr.name
  user_data_replace_on_change = true
  user_data = templatefile(
    "${path.module}/solr_server.user_data.sh", {
      deployments_bucket = var.deployments_bucket,
      solr_backup_bucket = var.solr_backup_bucket,
    }
  )
  tags = {
    Name          = "${var.application} ${var.customer} Solr"
    deployment_id = lower("${var.application}_${var.customer}_solr")
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_iam_role" "solr" {
  name = lower("${var.application}_${var.customer}_Solr")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow",
        }
      ]
    }
  )
}

resource "aws_iam_instance_profile" "solr" {
  name = lower("${var.application}_${var.customer}_Solr")
  role = aws_iam_role.solr.name
}


resource "aws_iam_role_policy_attachment" "solr_ssm" {
  role       = aws_iam_role.solr.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_iam_policy" "solr" {
  name = lower("${var.application}_${var.customer}_Solr")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" = "Allow",
          "Action" : "s3:ListBucket",
          "Resource" : "arn:aws:s3:::${var.deployments_bucket}"
        },
        {
          "Effect" = "Allow",
          "Action" : [
            "s3:GetObject",
            "s3:PutObject"
          ],
          "Resource" : [
            "arn:aws:s3:::${var.deployments_bucket}/*",
            "arn:aws:s3:::${var.solr_backup_bucket}/*"
          ]
        },
        {
          "Effect" = "Allow",
          "Action" : [
            "cloudwatch:PutMetricData"
          ],
          "Resource" : "*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "solr" {
  role       = aws_iam_role.solr.name
  policy_arn = aws_iam_policy.solr.arn
}

resource "aws_security_group" "solr" {
  name   = lower("${var.application}_${var.customer}_Solr")
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "${var.application} ${var.customer} Solr"
  }
  ingress {
    description = "Allow access to Solr form VPC"
    from_port   = 8983
    to_port     = 8983
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

output "solr_server_id" {
  value = aws_instance.solr.id
}
