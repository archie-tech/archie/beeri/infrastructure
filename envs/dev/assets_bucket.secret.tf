data "aws_s3_bucket" "secret_assets" {
  bucket = var.secret_assets_bucket
}

resource "aws_s3_bucket_versioning" "secret_assets" {
  bucket = data.aws_s3_bucket.secret_assets.id
  versioning_configuration {
    status = "Enabled"
  }
}

data "aws_s3_bucket" "secret_assets_backup" {
  provider = aws.acm
  bucket   = "${var.secret_assets_bucket}.backup"
}

resource "aws_s3_bucket_versioning" "secret_assets_backup" {
  provider = aws.acm
  bucket   = data.aws_s3_bucket.secret_assets_backup.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_iam_role" "secret_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_secret_assets_bucket_replication")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "s3.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "secret_assets_bucket_replication" {
  name = lower("${var.application}_${var.customer}_secret_assets_bucket_replication")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetReplicationConfiguration",
            "s3:ListBucket",
          ]
          "Resource" : [data.aws_s3_bucket.secret_assets.arn]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:GetObjectVersionForReplication",
            "s3:GetObjectVersionAcl",
            "s3:GetObjectVersionTagging",
          ]
          "Resource" : ["${data.aws_s3_bucket.secret_assets.arn}/*"]
        },
        {
          "Effect" : "Allow"
          "Action" : [
            "s3:ReplicateObject",
            #"s3:ReplicateDelete",
            "s3:ReplicateTags"
          ]
          "Resource" : ["${data.aws_s3_bucket.secret_assets_backup.arn}/*"]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "secret_assets_bucket_replication" {
  role       = aws_iam_role.secret_assets_bucket_replication.name
  policy_arn = aws_iam_policy.secret_assets_bucket_replication.arn
}

resource "aws_s3_bucket_replication_configuration" "secret_assets" {
  depends_on = [
    aws_s3_bucket_versioning.secret_assets,
    aws_s3_bucket_versioning.secret_assets_backup
  ]
  role   = aws_iam_role.secret_assets_bucket_replication.arn
  bucket = data.aws_s3_bucket.secret_assets.id
  rule {
    id     = "secret-1"
    status = var.bucket_replication_status
    destination {
      bucket        = data.aws_s3_bucket.secret_assets_backup.arn
      storage_class = "DEEP_ARCHIVE"
    }
  }
}

# replicate existing objects
# aws s3 sync --storage-class "DEEP_ARCHIVE" s3://secret_assets_bucket s3://secret_assets_bucket.backup
