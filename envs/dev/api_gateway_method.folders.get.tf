resource "aws_api_gateway_method" "get_folders" {
  rest_api_id   = aws_api_gateway_rest_api.public.id
  resource_id   = aws_api_gateway_resource.folders.id
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = aws_api_gateway_authorizer.archie.id
}

resource "aws_api_gateway_method_response" "get_folders" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.folders.id
  http_method = aws_api_gateway_method.get_folders.http_method
  status_code = "200"
  response_models = {
    "application/json" = "Empty"
  }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
  depends_on = [aws_api_gateway_method.get_folders]
}

resource "aws_api_gateway_integration" "get_folders" {
  rest_api_id             = aws_api_gateway_rest_api.public.id
  resource_id             = aws_api_gateway_resource.folders.id
  http_method             = aws_api_gateway_method.get_folders.http_method
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = aws_lambda_function.get_folders.invoke_arn
}

resource "aws_api_gateway_integration_response" "get_folders" {
  rest_api_id = aws_api_gateway_rest_api.public.id
  resource_id = aws_api_gateway_resource.folders.id
  http_method = aws_api_gateway_method.get_folders.http_method
  status_code = aws_api_gateway_method_response.get_folders.status_code
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
  depends_on = [aws_api_gateway_integration.get_folders]
}
