resource "aws_dynamodb_table" "reports" {
  name         = lower("${var.application}_${var.customer}_reports")
  hash_key     = "folder_id"
  billing_mode = "PAY_PER_REQUEST"
  #read_capacity  = 1
  #write_capacity = 10
  attribute {
    name = "folder_id"
    type = "S"
  }
  attribute {
    name = "creation_time"
    type = "S"
  }
  attribute {
    name = "report_month"
    type = "S"
  }
  global_secondary_index {
    name      = "report_month_index"
    hash_key  = "report_month"
    range_key = "creation_time"
    #write_capacity  = 10
    #read_capacity   = 1
    projection_type = "ALL"
  }
  tags = {
    Name = lower("${var.application}_${var.customer}_reports")
  }
}

resource "aws_lambda_function" "get_reports" {
  function_name = lower("${var.application}_${var.customer}_get_reports")
  role          = aws_iam_role.get_reports.arn
  runtime       = "python3.12"
  handler       = "app.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  environment {
    variables = {
      region         = var.aws_region
      logging_level  = "INFO"
      dynamodb_table = aws_dynamodb_table.reports.id
    }
  }
  lifecycle {
    ignore_changes = [s3_key]
  }
}

resource "aws_lambda_permission" "get_reports" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.get_reports.function_name
  principal     = "apigateway.amazonaws.com"
  source_arn = format(
    "arn:aws:execute-api:%s:%s:%s/*/%s%s",
    var.aws_region,
    var.aws_account,
    aws_api_gateway_rest_api.public.id,
    aws_api_gateway_method.get_reports.http_method,
    aws_api_gateway_resource.reports.path
  )
}

resource "aws_iam_role" "get_reports" {
  name = lower("${var.application}_${var.customer}_get_reports")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "get_reports_basic_execution" {
  role       = aws_iam_role.get_reports.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_policy" "get_reports" {
  name = lower("${var.application}_${var.customer}_get_reports")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "dynamodb:Scan",
            "dynamodb:PartiQLSelect",
            "dynamodb:GetItem"
          ],
          "Resource" : [
            aws_dynamodb_table.reports.arn,
            "${aws_dynamodb_table.reports.arn}/index/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "get_reports" {
  role       = aws_iam_role.get_reports.name
  policy_arn = aws_iam_policy.get_reports.arn
}
