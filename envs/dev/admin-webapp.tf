resource "aws_s3_bucket" "admin_webapp" {
  bucket = var.admin_webapp_domain_name
  tags = {
    Name = "${var.application} ${var.customer} Admin WebApp"
  }
}

resource "aws_s3_bucket_website_configuration" "admin_webapp" {
  bucket = aws_s3_bucket.admin_webapp.bucket
  index_document {
    suffix = "index.html"
  }
  error_document {
    key = "index.html"
  }
}

resource "aws_s3_bucket_acl" "admin_webapp" {
  bucket = aws_s3_bucket.admin_webapp.id
  acl    = "public-read"
}

resource "aws_s3_bucket_policy" "admin_webapp" {
  bucket = aws_s3_bucket.admin_webapp.id
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "PublicReadGetObject",
          "Effect" : "Allow",
          "Principal" : "*",
          "Action" : "s3:GetObject",
          "Resource" : "${aws_s3_bucket.admin_webapp.arn}/*"
        }
      ]
    }
  )
}

resource "aws_acm_certificate" "admin_webapp" {
  provider                  = aws.acm
  domain_name               = var.admin_webapp_domain_name
  subject_alternative_names = ["*.${var.admin_webapp_domain_name}"]
  validation_method         = "DNS"
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate_validation" "admin_webapp" {
  provider                = aws.acm
  certificate_arn         = aws_acm_certificate.admin_webapp.arn
  validation_record_fqdns = [for record in aws_route53_record.admin_webapp_validation : record.fqdn]
}

resource "aws_cloudfront_distribution" "admin_webapp" {
  origin {
    domain_name = aws_s3_bucket.admin_webapp.bucket_regional_domain_name
    origin_id   = var.admin_webapp_domain_name
  }
  aliases             = [var.admin_webapp_domain_name]
  enabled             = true
  is_ipv6_enabled     = true
  default_root_object = "index.html"

  custom_error_response {
    error_code         = 404
    response_code      = 200
    response_page_path = "/index.html"
  }

  default_cache_behavior {
    target_origin_id         = var.admin_webapp_domain_name
    viewer_protocol_policy   = "allow-all"
    min_ttl                  = 0
    default_ttl              = 0
    max_ttl                  = 0
    allowed_methods          = ["GET", "HEAD", "OPTIONS", "PUT", "POST", "PATCH", "DELETE"]
    cached_methods           = ["GET", "HEAD"]
    cache_policy_id          = "4135ea2d-6df8-44a3-9df3-4b5a84be39ad"
    origin_request_policy_id = "88a5eaf4-2fd4-4709-b370-b4c650ea3fcf"
  }

  restrictions {
    geo_restriction {
      restriction_type = "none"
    }
  }

  viewer_certificate {
    acm_certificate_arn = aws_acm_certificate_validation.admin_webapp.certificate_arn
    ssl_support_method  = "sni-only"
  }

}

resource "aws_route53_record" "admin_webapp" {
  zone_id = var.hosted_zone_id
  name    = var.admin_webapp_domain_name
  type    = "CNAME"
  ttl     = 60
  records = [aws_cloudfront_distribution.admin_webapp.domain_name]
}

resource "aws_route53_record" "admin_webapp_validation" {
  for_each = {
    for dvo in aws_acm_certificate.admin_webapp.domain_validation_options : dvo.domain_name => {
      name   = dvo.resource_record_name
      record = dvo.resource_record_value
      type   = dvo.resource_record_type
    }
  }
  allow_overwrite = true
  name            = each.value.name
  records         = [each.value.record]
  ttl             = 60
  type            = each.value.type
  zone_id         = var.hosted_zone_id
}
