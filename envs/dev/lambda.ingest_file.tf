resource "aws_lambda_function" "ingest_file" {
  function_name = lower("${var.application}_${var.customer}_ingest_file")
  role          = aws_iam_role.ingest_file.arn
  runtime       = "python3.12"
  handler       = "ingest_file.lambda_handler"
  s3_bucket     = var.deployments_bucket
  s3_key        = "place-holders/lambda/python/app.zip"
  memory_size   = 4096
  timeout       = 300
  ephemeral_storage {
    size = 10240 # Min 512 MB and the Max 10240 MB
  }
  vpc_config {
    subnet_ids         = [aws_subnet.private.id]
    security_group_ids = [aws_security_group.solr.id]
  }
  environment {
    variables = {
      region                    = var.aws_region
      environment               = var.environment
      secret                    = lower("${var.application}_${var.customer}")
      logging_level             = "INFO"
      ingest_bucket             = aws_s3_bucket.ingest.id
      dynamodb_table            = aws_dynamodb_table.reports.id
      queue_url                 = aws_sqs_queue.ingest_folder.url
      public_assets_bucket      = var.public_assets_bucket
      private_assets_bucket     = var.private_assets_bucket
      secret_assets_bucket      = var.secret_assets_bucket
      solr_url                  = "http://${aws_instance.solr.private_ip}:8983/solr/archie_beeri"
      archie_base64_credentials = var.archie_base64_credentials
    }
  }
  lifecycle {
    ignore_changes = [image_uri]
  }
}

resource "aws_lambda_event_source_mapping" "ingest_file" {
  function_name    = aws_lambda_function.ingest_file.arn
  event_source_arn = aws_sqs_queue.ingest_folder.arn
  batch_size       = 1
}

resource "aws_iam_role" "ingest_file" {
  name = lower("${var.application}_${var.customer}_ingest_file")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "AssumeRolePolicy",
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : [
              "lambda.amazonaws.com"
            ]
          },
          "Effect" : "Allow"
        }
      ]
    }
  )
}

resource "aws_iam_policy" "ingest_file" {
  name = lower("${var.application}_${var.customer}_ingest_file")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "secretsmanager:ListSecrets",
            "dynamodb:ListTables",
          ],
          "Resource" : "*"
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "sqs:ChangeMessageVisibility",
            "sqs:DeleteMessage",
            "sqs:GetQueueAttributes",
            "sqs:GetQueueUrl",
            "sqs:ReceiveMessage"
          ],
          "Resource" : aws_sqs_queue.ingest_folder.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "s3:*"
          ],
          "Resource" : [
            aws_s3_bucket.ingest.arn,
            "${aws_s3_bucket.ingest.arn}/*",
            "arn:aws:s3:::${var.public_assets_bucket}",
            "arn:aws:s3:::${var.public_assets_bucket}/*",
            "arn:aws:s3:::${var.private_assets_bucket}",
            "arn:aws:s3:::${var.private_assets_bucket}/*",
            "arn:aws:s3:::${var.secret_assets_bucket}",
            "arn:aws:s3:::${var.secret_assets_bucket}/*"
          ]
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "dynamodb:UpdateItem"
          ],
          "Resource" : aws_dynamodb_table.reports.arn
        },
        {
          "Effect" : "Allow",
          "Action" : [
            "secretsmanager:GetResourcePolicy",
            "secretsmanager:GetSecretValue",
            "secretsmanager:DescribeSecret",
            "secretsmanager:ListSecretVersionIds"
          ],
          "Resource" : "arn:aws:secretsmanager:${var.aws_region}:${var.aws_account}:secret:${lower("${var.application}_${var.customer}-??????")}"
        },
        {
          "Effect" : "Allow",
          "Action" : "lambda:InvokeFunction",
          "Resource" : "${aws_lambda_function.multimedia_utilities.arn}*"
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "ingest_file" {
  role       = aws_iam_role.ingest_file.name
  policy_arn = aws_iam_policy.ingest_file.arn
}

resource "aws_iam_role_policy_attachment" "ingest_file_basic_execution" {
  role       = aws_iam_role.ingest_file.id
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"
}

resource "aws_iam_role_policy_attachment" "ingest_file_eni_management" {
  role       = aws_iam_role.ingest_file.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaENIManagementAccess"
}
