#!/usr/bin/env python3

import re
import sys
import filecmp
import json
from pathlib import Path


def init_report(env1, env2):
    return {
        Path(env1).name: {
            "sub_folders": [],
            "invalid_files": [],
            "missing_tragets": [],
            "missing_var_keys": []
        },
        Path(env2).name: {
            "sub_folders": [],
            "invalid_files": [],
            "missing_tragets": [],
            "missing_var_keys": []
        },
        "changed_files": []
    }


def compare_resources(env1, env2, report):
    folder1 = Path(env1)
    folder2 = Path(env2)
    # env 1
    for p in folder1.iterdir():
        compare_resource(path=p, dir=folder2, report=report)
    # env 2
    for p in folder2.iterdir():
        compare_resource(path=p, dir=folder1, report=report)
    # sort
    report[folder1.name]["missing_tragets"].sort()
    report[folder2.name]["missing_tragets"].sort()
    report["changed_files"].sort()
    return report


def compare_resource(path, dir, report):
    # sub folder
    if path.is_dir():
        if not path.name in [".terraform"]:
            report[path.parent.name]["sub_folders"].append(path.name)
        return report
    # invalid file
    if not re.search("^[a-zA-Z0-9]+", path.name):
        if not path.name in [".terraform.lock.hcl"]:
            report[path.parent.name]["invalid_files"].append(path.name)
        return report
    # missing target
    target = dir / path.name
    if not target.exists():
        report[path.parent.name]["missing_tragets"].append(path.name)
        return report
    # changed file
    if not path.name in report["changed_files"]:
        if not filecmp.cmp(str(path), str(target), shallow=False):
            report["changed_files"].append(path.name)
    # done
    return report


def compare_variables(env1, env2, report):
    # read variable files
    with open(Path(env1) / "0.vars.tf.json", "r") as f:
        vars1 = json.load(f)
    with open(Path(env2) / "0.vars.tf.json", "r") as f:
        vars2 = json.load(f)
    # convert variables to simple dict
    dict1 = {}
    for k in vars1["variable"]:
        dict1[k] = vars1["variable"][k]["default"] if vars1["variable"][k] else ""
    dict2 = {}
    for k in vars2["variable"]:
        dict2[k] = vars2["variable"][k]["default"] if vars2["variable"][k] else ""
    # compare keys
    for k in dict1.keys():
        if not k in dict2.keys():
            report[Path(env2).name]["missing_var_keys"].append(k)
    for k in dict2.keys():
        if not k in dict1.keys():
            report[Path(env1).name]["missing_var_keys"].append(k)
    return report


if len(sys.argv) != 3:
    usage = "python script.py first_env second_env"
    example = "python {} {} {}".format(
        sys.argv[0], "envs/dev", "envs/prod")
    print("Usage: " + usage)
    print("Example: " + example)
    sys.exit(1)

report = init_report(env1=sys.argv[1], env2=sys.argv[2])
report = compare_resources(env1=sys.argv[1], env2=sys.argv[2], report=report)
report = compare_variables(env1=sys.argv[1], env2=sys.argv[2], report=report)
formatted_report = json.dumps(report, indent=2)
print(formatted_report)
