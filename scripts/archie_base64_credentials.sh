#!/bin/bash

archie_password=$(pwgen -1)
echo "generated password is $archie_password"

base64_credentials=$(echo -n "archie:$archie_password" | base64)
echo "base64 credentials are $base64_credentials"

decoded_credentials=$(echo ${base64_credentials} | base64 --decode)
echo "decoded credentials are $decoded_credentials"
