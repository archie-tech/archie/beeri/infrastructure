#!/bin/bash

aws ec2 describe-instances \
--query "Reservations[*].Instances[*].{ID:InstanceId,PublicIP:PublicIpAddress,Type:InstanceType,Name:Tags[?Key=='Name']|[0].Value,Status:State.Name}"
#--filters "Name=instance-state-name,Values=running" "Name=tag:Name,Values='*'" 

#  aws ec2 start-instances --instance-ids i-00965b37ab3dea019