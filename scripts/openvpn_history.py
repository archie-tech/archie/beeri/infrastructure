#!/usr/bin/env python3

def main_task(status_file, history_file):
    print(f"reading from {status_file} and writing to {history_file}")
    start_mark_found = False
    history = []
    # analyze status file
    with open(status_file) as f:
        lines = f.readlines()
    for line in lines:
        line = line.strip()
        if line.startswith("Common Name"):
            start_mark_found = True
            continue
        elif line.startswith("ROUTING TABLE"):
            break
        if start_mark_found:
            parts = line.split(",")
            common_name = parts[0]
            real_address = parts[1]
            connected_since = parts[4]
            print(f"{common_name} connected from {real_address} since {connected_since}")
            history.append((f"{common_name},{real_address},{connected_since}"))
    # append results to history file
    with open(history_file, 'a') as f:
        for line in history:
            f.write(line)
            f.write('\n')


if __name__ == "__main__":
    status_file = "local/openvpn/status.log"
    history_file = "local/openvpn/history.csv"
    main_task(status_file, history_file)
