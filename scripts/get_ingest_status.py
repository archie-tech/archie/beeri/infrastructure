#!/usr/bin/env python3

#!/usr/bin/env python3

import logging
import os
import json
import argparse
import boto3


def parse_arguments():
    epilog = "Example: get_ingest_status.py --month 2023-04"
    parser = argparse.ArgumentParser(description='This script get ingest status from dynamodb table.', epilog=epilog)
    parser.add_argument('--month', '-m', type=str, required=True, help='report month')
    args = parser.parse_args()
    return args


def get_status(month):
    logging.basicConfig(
        format='%(levelname)s : %(name)s : %(module)s.%(funcName)s : %(message)s',
        level=getattr(logging, os.getenv("logging_level", "INFO")),
        force=True
    )
    client = boto3.client('dynamodb')
    response = client.execute_statement(
        Statement="SELECT * FROM archie_beeri_reports.report_month_index WHERE report_month=?",
        Parameters=[
            {
                "S": month
            }
        ]
    )
    # print(json.dumps(response))
    for folder in response["Items"]:
        # print(json.dumps(item))
        for file_id in folder["files"]['M']:
            file = folder["files"]['M'][file_id]['M']
            print(file['ingest_status']['M']['action']['S'])


if __name__ == "__main__":
    args = parse_arguments()
    get_status(month=args.month)
