#!/usr/bin/env python3

import os
import json
import boto3


AWS_REGION = os.getenv("AWS_REGION", "eu-west-1")
AWS_SECRET = os.getenv("AWS_SECRET", "beeri_ip_set")


def get_secret():
    client = boto3.client('secretsmanager', region_name=AWS_REGION)
    response = client.get_secret_value(SecretId=AWS_SECRET)
    secret = json.loads(response['SecretString'])
    return secret


def create_variables_file(secret):
    beeri_ip_cidr = []
    beeri_ip = []
    for k in secret.keys():
        beeri_ip_cidr.append(secret[k])
        parts = secret[k].split('/')
        beeri_ip.append(parts[0])
    data = {
        "variable": {
            "beeri_ip_cidr": {"default": beeri_ip_cidr},
            "beeri_ip": {"default": ','.join(beeri_ip)}
        }
    }
    with open("0.beeri_ip_set.vars.tf.json", 'w') as out_file:
        json.dump(data, out_file, indent=4)


if __name__ == "__main__":
    print(f"retriving values from secret {AWS_SECRET}")
    secret = get_secret()
    create_variables_file(secret)
