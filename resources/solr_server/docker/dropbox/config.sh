#!/bin/bash

docker exec solr /opt/solr/bin/solr create -c dropbox

cmd="curl http://localhost:8983/solr/dropbox/schema -X POST -H 'Content-type:application/json' -d"
find schema -name "*.json" -exec $cmd @{} \;
