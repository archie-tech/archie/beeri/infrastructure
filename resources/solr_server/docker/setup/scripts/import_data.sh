#!/bin/bash

BASE_URL="http://localhost:8983/solr/archie_beeri"
DATA_FILE="/var/opt/data.json"

if [ ! -f $DATA_FILE ]; then
	echo "file $DATA_FILE not found"
fi

curl $BASE_URL/update -H "Content-Type: text/json" --data-binary @$DATA_FILE