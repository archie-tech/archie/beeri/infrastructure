#!/usr/bin/env python3

import requests
import json

BASE_URL = "http://localhost:8983/solr/archie_beeri"
FIELDS = "id,dcAccessRights,dcCreator,dcDate,dcDescription,dcFormat,dcIsPartOf,dcSubject,dcTitle,dcType,fileDigest,importTime,storageLocation"
DATA_FILE = "/var/opt/data.json"
ROWS = 1000 * 1000

r = requests.get("{}/select?q=*:*&fl={}&rows={}".format(BASE_URL, FIELDS, ROWS))
r.raise_for_status()
data = json.loads(r.text)
docs = data["response"]["docs"]
print("{} docs found".format(len(docs)))
with open(DATA_FILE, "w") as f:
    f.write(json.dumps(docs, indent=4))
