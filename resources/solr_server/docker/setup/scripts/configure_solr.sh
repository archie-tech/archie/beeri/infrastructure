#!/bin/bash

docker exec solr /opt/solr/bin/solr create -c archie_beeri

cmd="curl http://localhost:8983/solr/archie_beeri/schema -X POST -H 'Content-type:application/json' -d"
find /var/opt/setup/conf/solr-schema-base -name "*.json" -exec $cmd @{} \;
find /var/opt/setup/conf/solr-schema-copy -name "*.json" -exec $cmd @{} \;

curl http://localhost:8983/solr/archie_beeri/config -X POST -H 'Content-type:application/json' -d '{"set-user-property": {"update.autoCreateFields":"false"}}'
curl http://localhost:8983/solr/archie_beeri/config -X POST -H 'Content-type:application/json' -d '{"set-property":{"updateHandler.autoCommit.maxTime":60000}}'
curl http://localhost:8983/solr/archie_beeri/config -X POST -H 'Content-type:application/json' -d '{"set-property":{"updateHandler.autoSoftCommit.maxTime":5000}}'

curl http://localhost:8983/solr/archie_beeri/config -X POST -H 'Content-type:application/json' -d @/var/opt/setup/conf/solr.request-handler.select.json
curl http://localhost:8983/solr/archie_beeri/config -X POST -H 'Content-type:application/json' -d @/var/opt/setup/conf/solr.request-handler.replication.json
