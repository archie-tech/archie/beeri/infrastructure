Label the Linux block device, so it can be mounted later by label and not by device name:

* Attache the volume to to a Linux EC2 instance
* SSH to the Linux instance
* Find out the block device name using the command `lsblk` (e.g. */dev/nvme1n1*)
* Build a Linux filesystem on the device, using the command `mkfs` (e.g. `mkfs -t xfs /dev/nvme1n1`)
* Change the label on the file system to *solr-data*, using the command `xfs_admin` (e.g. `xfs_admin -L solr-data /dev/nvme1n1`)
