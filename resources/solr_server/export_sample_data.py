#!/usr/bin/env python3

import requests
import json

BASE_URL = "http://localhost:8983/solr/archie_beeri"
FIELDS = "id,dcAccessRights,dcCreator,dcDate,dcDescription,dcFormat,dcIsPartOf,dcSubject,dcTitle,dcType,fileDigest,importTime,storageLocation"
DATA_FILE = "/var/opt/solr-sample-data.json"
ROWS = 1000


def export_public_docs():
    r = requests.get("{}/select?q=dcAccessRights:public&fl={}&rows={}".format(BASE_URL, FIELDS, ROWS))
    r.raise_for_status()
    data = json.loads(r.text)
    docs = data["response"]["docs"]
    print("{} docs found".format(len(docs)))
    with open(DATA_FILE, "w") as f:
        f.write(json.dumps(docs, indent=4))


def export_collections():
    filter = "dcTitle:{} or dcTitle:{}".format("כללי","קיבוץ קדמה")
    r = requests.get("{}/select?q=dcType:collection&fq={}&fl={}&rows={}".format(BASE_URL, filter, FIELDS, ROWS))
    r.raise_for_status()
    data = json.loads(r.text)
    docs = data["response"]["docs"]
    print("{} docs found".format(len(docs)))
    with open("/var/opt/solr-collecions.json", "w") as f:
        f.write(json.dumps(docs, indent=4))


export_collections()
