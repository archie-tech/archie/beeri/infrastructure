#!/bin/bash

AVAILABILITY_ZONE="eu-west-1a" # data volume should be in the same zone as solr ec2 instance
AWS_PROFILE="arinamal-dev"

aws ec2 create-volume \
    --profile $AWS_PROFILE \
    --region eu-west-1 \
    --availability-zone $AVAILABILITY_ZONE \
    --size 8 \
    --tag-specifications 'ResourceType=volume,Tags=[{Key=Name,Value=archie_beeri_solr_data}]'
