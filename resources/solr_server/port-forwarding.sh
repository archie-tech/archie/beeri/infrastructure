#!/bin/bash

# Start port forwarding session from developer work-station to remote host running Solr

if [ "$#" -ne 1 ] ; then
  echo "Usage: $0 aws_profile" >&2
  exit 1
fi

export aws_profile=$1
export instance_id=$(terraform output -raw solr_server_id)

aws ssm start-session \
--region eu-west-1 \
--profile $aws_profile \
--target $instance_id \
--document-name AWS-StartPortForwardingSession \
--parameters "localPortNumber=8983,portNumber=8983"
