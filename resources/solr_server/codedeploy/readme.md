# Create deployment

0. Download and extract latest version of Apache Solr
0. Add the content of this folder
0. Create zip file and name it vX.Y.Z_b1 (x.y.z is the same as Solr version, b1 means build #1).
0. Upload vX.Y.Z_b1.zip to deployments bucket (example: s3://deployments.dev.arinamal.com/archie_beeri_solr)
0. AWS web console: CodeDeploy > Applications > archie_beeri_solr > deployments > Create deployment
0. First deployment on instance
   * ApplicationStop lifecycle event failure - optional
     * Don't fail the deployment to an instance if this lifecycle event on the instance fails

# Backup and restore

* curl http://localhost:8983/solr/archie_beeri/replication?command=backup
* curl http://localhost:8983/solr/archie_beeri/replication?command=restore