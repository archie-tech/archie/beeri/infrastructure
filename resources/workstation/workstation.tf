resource "aws_instance" "workstation" {
  ami                         = var.ami
  instance_type               = var.instance_type
  iam_instance_profile        = aws_iam_instance_profile.workstation.name
  key_name                    = lower("${var.application}_${var.customer}_${var.environment}")
  associate_public_ip_address = true
  subnet_id                   = var.subnet_id
  user_data_replace_on_change = true
  user_data                   = templatefile("${path.module}/workstation.user_data.sh", {})
  tags = {
    Name       = lower("${var.application}_${var.customer}_workstation")
    auto_start = "no"
    auto_stop  = "yes"
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_eip" "workstation" {
  instance = aws_instance.workstation.id
  tags = {
    Name = lower("${var.application}_${var.customer}_workstation")
  }
}

resource "aws_iam_instance_profile" "workstation" {
  name = lower("${var.application}_${var.customer}_workstation")
  role = aws_iam_role.workstation.name
}

resource "aws_iam_role" "workstation" {
  name = lower("${var.application}_${var.customer}_workstation")
  assume_role_policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Action" : "sts:AssumeRole",
          "Principal" : {
            "Service" : "ec2.amazonaws.com"
          },
          "Effect" : "Allow",
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "workstation_ssm" {
  role       = aws_iam_role.workstation.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore"
}

resource "aws_s3_bucket" "workstation_storage" {
  bucket = lower("${var.application}-${var.customer}-${var.environment}-workstation")
  tags = {
    name = lower("${var.application}_${var.customer}_workstation")
  }
}

resource "aws_iam_policy" "workstation_storage" {
  name = lower("${var.application}_${var.customer}_${var.environment}_workstation_storage")
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "Sync",
          "Effect" : "Allow",
          "Action" : [
            "s3:ListBucket",
            "s3:GetBucketLocation",
            "s3:GetObject",
            "s3:PutObject",
            "s3:DeleteObject"
          ],
          "Resource" : [
            "${aws_s3_bucket.workstation_storage.arn}",
            "${aws_s3_bucket.workstation_storage.arn}/*"
          ]
        }
      ]
    }
  )
}

resource "aws_iam_role_policy_attachment" "workstation_storage" {
  role       = aws_iam_role.workstation.name
  policy_arn = aws_iam_policy.workstation_storage.arn
}

output "work_station_id" {
  value = aws_instance.workstation.id
}
