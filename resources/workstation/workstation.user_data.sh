#!/bin/bash -xe
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1
apt-get -y update

# Install AWS CLI

apt-get -y install unzip
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install

# Install and configure VNC server
apt-get -y install xfce4 xfce4-goodies tigervnc-standalone-server pwgen
mkdir /home/ubuntu/.vnc
# startup script
cat << EOF >> /home/ubuntu/.vnc/xstartup
#!/bin/sh
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
/usr/bin/startxfce4
[ -x /etc/vnc/xstartup ] && exec /etc/vnc/xstartup
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources
x-window-manager &
EOF
chmod +x /home/ubuntu/.vnc/xstartup
# password
p=`pwgen`
echo $p | vncpasswd -f > /home/ubuntu/.vnc/passwd
chmod 600 /home/ubuntu/.vnc/passwd
echo $p > /var/opt/vnc_password.txt
# cleanup
chown -R ubuntu:ubuntu /home/ubuntu/.vnc

# install google chrome

#apt-get -y install software-properties-common apt-transport-https wget ca-certificates gnupg2 ubuntu-keyring
wget -O- https://dl.google.com/linux/linux_signing_key.pub | gpg --dearmor | tee /usr/share/keyrings/google-chrome.gpg
echo deb [arch=amd64 signed-by=/usr/share/keyrings/google-chrome.gpg] http://dl.google.com/linux/chrome/deb/ stable main | tee /etc/apt/sources.list.d/google-chrome.list
apt-get -y update
apt-get -y install google-chrome-stable

# Finish

echo "user-data script done"
