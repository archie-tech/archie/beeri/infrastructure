# Connect to remote EC2 desktop from local PC

Setup

* Install AWS CLI
* Install the Session Manager plugin for the AWS CLI
* Install TigerVNC Viewer

Set variables

* instance_id
* aws_profile
* aws_region

## Start VNC server on the remote instance

aws ssm send-command --document-name "AWS-RunShellScript" --parameters 'commands=["runuser -u ubuntu -- vncserver -localhost no :1"]' --targets "Key=instanceids,Values=$instance_id"

## Strart session from local PC to remote instance

aws ssm start-session --document-name AWS-StartPortForwardingSession --parameters "localPortNumber=5901,portNumber=5901" --target "$instance_id"

## Connect

* TigerVNC - localhost:5901

# Remote EC2 instance post install tasks

* Set password for ubuntu user - same as vnc
* Disable screen saver (optional)

## Useful commands

* vncserver -localhost no :1
* vncserver -kill