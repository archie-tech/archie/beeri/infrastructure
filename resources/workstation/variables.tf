variable "application" {}
variable "customer" {}
variable "environment" {}

variable "subnet_id" {}

variable "graviton_nitro_instance" {
  default     = "t4g.xlarge" # vCPU: 4, Memory: 16 GB
  description = "T4g instances are powered by AWS Graviton2 processors and built on the AWS Nitro System. Instance Size xlarge"
}

variable "graviton_nitro_ami" {
  default     = "ami-090b049bea4780001"
  description = "Canonical, Ubuntu, 22.04 LTS, arm64 jammy image build on 2023-05-16 in region eu-west-1"
}

variable "instance_type" {
  default = "t3.xlarge" # vCPU: 4, Memory: 16 GB
}

variable "ami" {
  default     = "ami-01dd271720c1ba44f"
  description = "Canonical, Ubuntu, 22.04 LTS, amd64 jammy image build on 2023-05-16"
}
