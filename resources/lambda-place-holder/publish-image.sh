#!/bin/bash


# Create ECR repository 
# export: aws_account aws_region

aws ecr create-repository --repository-name lambda_python_placeholder
sudo docker build --rm --tag lambda_python_placeholder .
sudo docker tag lambda_python_placeholder:latest $aws_account.dkr.ecr.$aws_region.amazonaws.com/lambda_python_placeholder:latest
aws ecr get-login-password --region $aws_region | sudo docker login --username AWS --password-stdin $aws_account.dkr.ecr.$aws_region.amazonaws.com
sudo docker push $aws_account.dkr.ecr.$aws_region.amazonaws.com/lambda_python_placeholder:latest
